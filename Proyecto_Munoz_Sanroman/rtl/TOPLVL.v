module TOP(
    clk,
    reset,
    clk_out
    );

input clk;
input reset;
output clk_out;

clk_div CLOCKDIV(
    .clk(clk),
    .reset(reset),
    .clk_out(clk_out)
);
endmodule
