module TOP(
    Clk                     			,
	Rst_n                   			,   
   	Rx                      			,    
    Tx                      			,
	Trigger								,
	Echo								
	);

/////////////////////////////////////////////////////////////////////////////////////////
input           Clk             ; // Clock.
input           Rst_n           ; // Reset.
input           Rx              ; // RS232 RX line.
input 			Echo			;
output          Tx              ; // RS232 TX line.
output			Trigger			; // Trigger
/////////////////////////////////////////////////////////////////////////////////////////
wire [7:0]    	TxData     		; // Data to transmit.
wire          	RxDone          ; // Reception completed. Data is valid.
wire          	TxDone          ; // Trnasmission completed. Data sent.
wire            tick			; // Baud rate clock
wire          	TxEn        	;
wire			Clk_div			;
wire 			RxEn			;
wire [3:0]      NBits    		;
wire [15:0]     BaudRate        ; //328; 162 etc... (Read comment in baud rate generator file)
wire [7:0]		Distancia		; // Distancia.
wire [7:0]    	RxData          ; // Received data.
	
/////////////////////////////////////////////////////////////////////////////////////////
assign 		RxEn = 1'b1	;
assign 		BaudRate = 16'd325; 	//baud rate set to 9600 for the HC-06 bluetooth module. Why 325? (Read comment in baud rate generator file)
assign 		NBits = 4'b1000	;	//We send/receive 8 bits
/////////////////////////////////////////////////////////////////////////////////////////


//Make connections between Rx module and TOP inputs and outputs and the other modules
UART_rs232_rx I_RS232RX(
    .Clk(Clk)             	,
   	.Rst_n(Rst_n)         	,
    .RxEn(RxEn)           	,
    .RxData(RxData)       	,
    .RxDone(RxDone)       	,
    .Rx(Rx)               	,
    .Tick(tick)           	,
    .NBits(NBits)
    );

//Make connections between Tx module and TOP inputs and outputs and the other modules
UART_rs232_tx I_RS232TX(
   	.Clk(Clk)            	,
    .Rst_n(Rst_n)         	,
    .TxEn(TxEn)           	,
    .TxData(Distancia)      ,
   	.TxDone(TxDone)      	,
   	.Tx(Tx)               	,
   	.Tick(tick)           	,
   	.NBits(NBits)
    );

//Make connections between tick generator module and TOP inputs and outputs and the other modules
UART_BaudRate_generator I_BAUDGEN(
    .Clk(Clk)               ,
    .Rst_n(Rst_n)           ,
    .Tick(tick)             ,
    .BaudRate(BaudRate)
    );

clk_div CLOCKDIV(
    .clk(Clk),
    .reset(Rst_n),
    .clk_out(Clk_div)
);
sonic sc(
	.clock(Clk),
	.echo(Echo),
	.distance(Distancia),
	.trig(Trigger)
	);

controller control(
	.clock(Clk),
	.RxData(RxData),
	.ctrl(TxEn),
	.Clk_div(Clk_div),
	.Rst_n(Rst_n)
);
endmodule
