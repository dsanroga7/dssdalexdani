module clk_div (clk,reset, clk_out);
 
input clk;
input reset;
output reg clk_out;
reg [31:0] contador=0;

 
always@(posedge clk or negedge reset) begin //ALWAYS ON ES DEFINIRA EL CANVI ENTRE ESTATS EN CADA CANVIA (FLANC DE PUJADA) DEL RELLOTGE I CADA CANVI
                                          //(FLANC DE BAIXADA) DEL RESET.
    if (!reset) begin //SI ES PULSA EL RESET TORNAREM AL ESTAT DE DISSENY.
        clk_out <= 0;
		  contador = 0;
    end
    else begin //SI NO ES PULSA EL RESET AL REGISTRE STATE SE LI PASSARA EL SEGUENT ESTAT.
        if (contador == 5999999) begin
				contador = 0;
				clk_out <= !clk_out;
		  end  
		  else begin
				contador <= contador + 1;
		  end
    end
end
endmodule