 module clkdiv2n_tb;
  reg clk,reset;
  wire clk_out;
 
     clk_div t1(clk,reset,clk_out);
        initial
          clk= 1'b0;
     always
        #5  clk=~clk; 
        initial
            begin
               #5 reset=1'b0;
               #10 reset=1'b1;
            end
 
        initial
               $monitor("clk=%b,reset=%b,clk_out=%b",clk,reset,clk_out);
 
        initial
            begin
              $dumpfile("clkdiv2n_tb.vcd");
              $dumpvars(0,clkdiv2n_tb);
             end
    endmodule