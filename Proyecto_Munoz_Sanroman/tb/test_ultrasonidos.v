`timescale 1ns/10ps
module test_ultrasonidos;
    reg rst;
    reg clock = 1'b0;
    reg strt = 1'b0;
    wire pulse = 1'b0;
    wire fin = 1'b0;
    wire dist = 13'b0;
    reg eco = 1'b0;
    

always #50 clock = ~ clock; //FEM UN RELLOTGE QUE ES MANTIGUI DE MANERA CICLICA UN TEMPS DE 100 EN ESTAT
                        // ALT I UN 100 EN ESTAT BAIX

// to end simulation
	//initial #10000 $stop;

//uut MASTER instantiation
    ultrasonidos ULT (
        .reset(rst),
        .clk(clock),
        .done(fin),
        .start(strt),
        .pulso(pulse),
        .distancia(dist),
        .echo(eco)
        );

      
// timed contrl signals
initial begin
  
  //////////////////////////////////////////
        #100 rst = 1'b1;
        #100 rst = 1'b0;
        #100 rst = 1'b1;
        strt = 1'b0;
        #500;  
        strt = 1'b1;
        //#500  strt = 1'b0;
        #6000  eco = 1'b1;
        #100  eco = 1'b0;
        #2500;
        strt = 1'b1;
        #500  strt = 1'b0;
        #600  eco = 1'b1;
        #100  eco = 1'b0;



        
        
        #10000 $stop;
    end
endmodule