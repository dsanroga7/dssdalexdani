library verilog;
use verilog.vl_types.all;
entity ultrasonidos is
    generic(
        INIT            : vl_logic_vector(0 to 1) := (Hi0, Hi0);
        ENVIO           : vl_logic_vector(0 to 1) := (Hi0, Hi1);
        RECIBO          : vl_logic_vector(0 to 1) := (Hi1, Hi0)
    );
    port(
        pulso           : out    vl_logic;
        echo            : in     vl_logic;
        start           : in     vl_logic;
        distancia       : out    vl_logic_vector(3 downto 0);
        reset           : in     vl_logic;
        clk             : in     vl_logic;
        done            : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of INIT : constant is 1;
    attribute mti_svvh_generic_type of ENVIO : constant is 1;
    attribute mti_svvh_generic_type of RECIBO : constant is 1;
end ultrasonidos;
